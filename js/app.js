var app = angular.module("routerApp",['ngRoute']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
    .when('/', {
        templateUrl : 'home.html'
    })
    .when('/about', {
        templateUrl : 'about.html'
    })
    .when('/contact', {
        templateUrl : 'contact.html'
    });

}]);


app.controller("contractCrlt", ["$scope", function($scope){


    $scope.contracts=[
        { 
            'name' : 'Rohim',
            'phone' : '01236489'
         },
        {
            'name': "probas",
            'phone' : '0174407234'
        },
        {
            'name': "saju",
            'phone' : '890723583'
        },
        {
            'name': "kamal",
            'phone' : '6317321'
        },
        {
            'name': "borhan",
            'phone' : '4761276'
        },
        {
            'name': "chaki",
            'phone' : '76416'
        },
        {
            'name': "arif",
            'phone' : '4758965876'
        },
        {
            'name': "hirok",
            'phone' : '0876765'
        },
        {
            'name': "sojib biswasa",
            'phone' : '537598'
        },
        {
            'name': "harun",
            'phone' : '35467598'
        },

    ]
}]);

app.filter('capatilize', function(){
    return function (value){
        var i, c, txt = "";
        for (i = 0; i < value.length; i++) {
            c = value[i];
            if (i == 0 || value[i-1]==' ') {
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    }
})

app.filter('formatnumber', function(){
    return function (value, forn){
        var i, c, txt = "";
        if (forn =='int'){
            for (i = 0; i < value.length; i++) {
            c = value[i];
            if (i == 2 || i==6) {
                c = c + '-';
            }
            txt += c;
        }
    }
        else if (forn =='local'){
        for (i = 0; i < value.length; i++) {
            c = value[i];
            if (i == 4 ) {
                c = c + '-';;
            }
            txt += c;
        }
        }
        return txt;
    }
    
})


app.factory('MyService', function () {
    var x=10;
    var setdata = function(){
        x = x+6;
    } 
    var getdata = function(){
        return x+this.y;
    }
    var data={
        y: 15,
        setdata: setdata,
        getdata: getdata,
    }
    
});